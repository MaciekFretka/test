package com.scruum.mastersofscruum.controller;

import com.scruum.mastersofscruum.model.User;
import com.scruum.mastersofscruum.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;




@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class LoginController {


    private final  UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public LoginController(UserRepository userRepository,BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    /**LOGIN**/
    @RequestMapping(value = "/loginUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> loginUser(String email, String password) {
        User user = userRepository.findByEmailAndPassword(email,password);
        if(user == null){
            return new ResponseEntity("User not found", HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    /**REGISTRATION**/
    @PostMapping(value = "/RegistrationUser", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> registrationUser(@RequestBody User user) {
        if(userRepository.findByEmail(user.getEmail()) != null){
            return new ResponseEntity("User exists", HttpStatus.FOUND);
        }
        User newUser = new User();
        user.setId(newUser.getId());
        //user.setPassword(passwordEncoder.encode(user.getPassword()));
        user = userRepository.save(user);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
}
