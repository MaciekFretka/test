package com.scruum.mastersofscruum.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scruum.mastersofscruum.model.ERole;
import com.scruum.mastersofscruum.repository.RoleRepository;
import com.scruum.mastersofscruum.repository.UserRepository;


@RestController
@RequestMapping("/api/apiRoles")
public class RoleController {

        @Autowired
        UserRepository userRepository;

        @Autowired
        RoleRepository roleRepository;

    @GetMapping(value = "/roles", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ERole>> getRoleUsers() {
        List<ERole> listRoles = new ArrayList<>();
        listRoles.add(ERole.ROLE_USER);
        listRoles.add(ERole.ROLE_ADMIN);
        listRoles.add(ERole.ROLE_MODERATOR);
        return new ResponseEntity<List<ERole>>(listRoles, HttpStatus.OK);
    }

}
