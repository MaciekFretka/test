package com.scruum.mastersofscruum.controller;

import com.scruum.mastersofscruum.dto.SprintDto;
import com.scruum.mastersofscruum.mapper.SprintMapper;
import com.scruum.mastersofscruum.model.Sprint;
import com.scruum.mastersofscruum.service.SprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class SprintController {

    @Autowired
    SprintService sprintService;
    @Autowired
    SprintMapper sprintMapper;

    @Deprecated
    private List<Sprint> getAllSprints(Integer startIndex, Integer startIndexOffset) {
        int tmp = (int)sprintService.sprintRepository().count();
        return sprintService.sprintRepository().findAll().subList(startIndex > tmp ? 0 : startIndex, startIndexOffset > tmp ? tmp : startIndexOffset);
    }

    @Deprecated
    @GetMapping(value = "/sprints/getAllWithoutDTO", produces = MediaType.APPLICATION_JSON_VALUE)
    private  ResponseEntity<List<Sprint>> getAllWithoutDTO() {
        return new ResponseEntity<List<Sprint>>(sprintService.sprintRepository().findAll(), HttpStatus.OK);
    }

    @PostMapping(value = "teams/{teamID}/sprints", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SprintDto> createSprint(@PathVariable String teamID, @RequestBody @Valid SprintDto sprintDto) {
        Sprint sprint = sprintMapper.fromDto(sprintDto);
        sprint = sprintService.save(teamID, sprint);
        sprintDto = sprintMapper.toDto(sprint);
        if (sprint != null && sprintService.sprintRepository().existsById(sprintDto.getId())) {
            return new ResponseEntity<SprintDto>(sprintDto, HttpStatus.CREATED);
        }
        return new ResponseEntity<SprintDto>(sprintDto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping(value = "/teams/{teamID}/sprints", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SprintDto>> getTeams(@PathVariable(required = false) String teamID, @RequestParam(required = false, defaultValue = "0") Integer startIndex, @RequestParam(required = false, defaultValue = "0") Integer startIndexOffset) {
        List<Sprint> sprints = new ArrayList<Sprint>();
        if (startIndexOffset == 0){
            startIndexOffset = Integer.MAX_VALUE;
        }
        if (teamID != null && !teamID.equals("")) {
            List<Sprint> tmp = sprintService.findByTeamId(teamID);
            sprints.addAll(tmp.subList(startIndex > tmp.size() ? 0 : startIndex, startIndexOffset > tmp.size() ? tmp.size() : startIndexOffset));
        }
        if (teamID == null || teamID.equals("") || teamID.equals("-1")) {
            int tmp = (int)sprintService.sprintRepository().count();
            sprints.addAll(sprintService.sprintRepository().findAll().subList(startIndex > tmp ? 0 : startIndex, startIndexOffset > tmp ? tmp : startIndexOffset)); // do usunięcia
        }
        if (sprints.size() == 0) {
            return new ResponseEntity("No teams found", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<SprintDto>>(sprintMapper.toDtoList(sprints), HttpStatus.OK);
    }

    @GetMapping(value = {"/teams/{teamID}/sprints/{sprintID}", "/sprints/{sprintID}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SprintDto> getSprint(@PathVariable String sprintID) {
        Optional<Sprint> optionalSprint = sprintService.sprintRepository().findById(sprintID);
        if (optionalSprint.isEmpty()) {
            return new ResponseEntity( "Sprint with index" + sprintID + "not found",HttpStatus.NO_CONTENT);
        }
        Sprint team = optionalSprint.get();
        SprintDto sprintDto = sprintMapper.toDto(team);
        return new ResponseEntity<SprintDto>(sprintDto, HttpStatus.OK);
    }

    @PutMapping(value = {"/teams/{teamID}/sprints/{sprintID}", "/sprints/{sprintID}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SprintDto> updateSprint(@PathVariable String sprintID, @RequestBody @Valid SprintDto sprintDto) {
        Optional<Sprint> optionalSprint = sprintService.sprintRepository().findById(sprintID);
        if (optionalSprint.isEmpty()) {
            return new ResponseEntity<SprintDto>(sprintDto, HttpStatus.NO_CONTENT);
        }
        Sprint sprint = sprintMapper.fromDto(sprintDto);
        sprint.setId(sprintID);
        sprint = sprintService.sprintRepository().save(sprint);
        sprintDto = sprintMapper.toDto(sprint);
        return new ResponseEntity<SprintDto>(sprintDto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/teams/{teamID}/sprints/{sprintID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SprintDto> deleteTeam(@PathVariable String teamID, @PathVariable String sprintID) {
        if (sprintService.deleteById(teamID, sprintID)) { // add check team sprint list
            return new ResponseEntity(null, HttpStatus.OK);
        }
        return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
    }
}
