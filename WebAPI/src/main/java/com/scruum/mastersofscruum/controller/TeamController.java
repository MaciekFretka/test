package com.scruum.mastersofscruum.controller;

import com.scruum.mastersofscruum.dto.TeamDto;
import com.scruum.mastersofscruum.mapper.TeamMapper;
import com.scruum.mastersofscruum.model.Role;
import com.scruum.mastersofscruum.model.Team;
import com.scruum.mastersofscruum.model.User;
import com.scruum.mastersofscruum.repository.UserRepository;
import com.scruum.mastersofscruum.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/teams")
public class TeamController {

    @Autowired
    TeamService teamService;
    @Autowired
    TeamMapper teamMapper;
    @Autowired
    UserRepository userRepository;


    @Deprecated
    @GetMapping(value = "/getAllWithoutDTO", produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<List<Team>> getAllWithoutDTO() {
        return new ResponseEntity<List<Team>>(teamService.teamRepository().findAll(), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TeamDto> createTeam(@RequestBody @Valid TeamDto teamDto) throws InstantiationException, IllegalAccessException {
        Team team = teamMapper.fromDto(teamDto);
        for (List<String> value : team.getMembers().values()) {
            for (int i = 0; i < value.size(); i++) {
                if (!userRepository.existsById(value.get(i))) {
                    return new ResponseEntity("User with id " + value.get(i) + " not exist", HttpStatus.NOT_FOUND);
                }
            }
        }
        team = teamService.teamRepository().save(team);
        teamDto = teamMapper.toDto(team);
        return new ResponseEntity<TeamDto>(teamDto, HttpStatus.CREATED);
    }


    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TeamDto>> getTeams(@RequestParam(required = false, defaultValue = "0") Integer startIndex, @RequestParam(required = false, defaultValue = "0") Integer startIndexOffset, @RequestParam(required = false) String teamName) {
        List<Team> teams = new ArrayList<Team>();
        if (startIndexOffset == null || startIndexOffset == 0) {
            startIndexOffset = Integer.MAX_VALUE;
        }
        if (teamName != null && !teamName.equals("")) {
            List<Team> tmp = teamService.teamRepository().findByTeamName(teamName);
            teams.addAll(tmp.subList(startIndex > tmp.size() ? 0 : startIndex, startIndexOffset > tmp.size() ? tmp.size() : startIndexOffset));
        }
        if (teamName == null || teamName.equals("")) {
            int tmp = (int) teamService.teamRepository().count();
            teams.addAll(teamService.teamRepository().findAll().subList(startIndex > tmp ? 0 : startIndex, startIndexOffset > tmp ? tmp : startIndexOffset));
        }
        if (teams.size() == 0) {
            return new ResponseEntity("No teams found", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<TeamDto>>(teamMapper.toDtoList(teams), HttpStatus.OK);
    }

    @GetMapping(value = "/{teamID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TeamDto> getTeam(@PathVariable String teamID) {
        Optional<Team> optionalTeam = teamService.teamRepository().findById(teamID);
        if (optionalTeam.isEmpty()) {
            return new ResponseEntity("No teams found", HttpStatus.BAD_REQUEST);
        }
        Team team = optionalTeam.get();
        TeamDto teamDto = teamMapper.toDto(team);
        return new ResponseEntity<TeamDto>(teamDto, HttpStatus.OK);
    }

    @GetMapping(value = "{teamID}/members", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Role>> getMembers(@PathVariable String teamID) {
        Optional<Team> optionalTeam = teamService.teamRepository().findById(teamID);
        if (optionalTeam.isEmpty()) {
            return new ResponseEntity("There are no users in the team", HttpStatus.BAD_REQUEST);
        }
        Team team = optionalTeam.get();
        Map<Role, List<String>> members = team.getMembers();
        Map<String, Role> usersInfo = new HashMap<>();

        if (members == null) {
            return new ResponseEntity("There are no users in the team", HttpStatus.BAD_REQUEST);
        } else {
            for (Map.Entry<Role, List<String>> entry : members.entrySet()) {
                for (int i = 0; i < entry.getValue().size(); i++) {
                    Optional<User> user = userRepository.findById(entry.getValue().get(i));
                    usersInfo.put(user.get().getEmail(), entry.getKey());
                }
            }
        }
        return new ResponseEntity<Map<String, Role>>(usersInfo, HttpStatus.OK);
    }


    @PutMapping(value = "/{teamID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TeamDto> updateTeam(@PathVariable String teamID, @RequestBody @Valid TeamDto teamDto) {
        Optional<Team> optionalTeam = teamService.teamRepository().findById(teamID);
        if (optionalTeam.isEmpty()) {
            return new ResponseEntity<TeamDto>(teamDto, HttpStatus.NO_CONTENT);
        }
        for (List<String> value : teamDto.getMembers().values()) {
            for (int i = 0; i < value.size(); i++) {
                if (!userRepository.existsById(value.get(i))) {
                    return new ResponseEntity("User with id " + value.get(i) + " not exist", HttpStatus.NOT_FOUND);
                }
            }
        }
        for (Role role : teamDto.getMembers().keySet()) {
            teamDto.getMembers().put(role,  teamDto.getMembers().get(role).stream().distinct().collect(Collectors.toList()));
        }
        Team team = teamService.updateTeam(optionalTeam.get(), teamDto);
        TeamDto teamDto1 = teamMapper.toDto(team);
        return new ResponseEntity<TeamDto>(teamDto1, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{teamID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TeamDto> deleteTeam(@PathVariable String teamID) {
        if (teamService.teamRepository().existsById(teamID)) {
            teamService.teamRepository().deleteById(teamID);
            return new ResponseEntity(null, HttpStatus.OK);
        }
        return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "/{teamID}$", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TeamDto> getDeleteUserFromTeam(@PathVariable String teamID, @RequestParam String removeUser) {
        Optional<Team> optionalTeam = teamService.teamRepository().findById(teamID);
        if (optionalTeam.isEmpty()) {
            return new ResponseEntity("No teams found", HttpStatus.BAD_REQUEST);
        }
        Team team = optionalTeam.get();
        for (Role role : team.getMembers().keySet()) {
            if (role == Role.SCRUM_MASTER && team.getMembers().get(role).contains(removeUser)) {
                return new ResponseEntity("You can't removed SCRUM_MASTER", HttpStatus.BAD_REQUEST);
            }
            team.getMembers().get(role).remove(removeUser);
        }
        team = teamService.teamRepository().save(team);
        TeamDto teamDto1 = teamMapper.toDto(team);
        return new ResponseEntity<TeamDto>(teamDto1, HttpStatus.OK);
    }

//    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<List<Team>> getAllTeams() {
//        List<Team> teams = teamRepository.findAll();
//        if (teams == null) {
//            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
//        }
//        return new ResponseEntity<List<Team>>(teams, HttpStatus.OK);
//    }

}
