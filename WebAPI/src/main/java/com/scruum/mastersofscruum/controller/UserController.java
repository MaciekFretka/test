package com.scruum.mastersofscruum.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.scruum.mastersofscruum.model.User;
import com.scruum.mastersofscruum.repository.RoleRepository;
import com.scruum.mastersofscruum.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;
   /* @Autowired
    UserService userService;*/

    @RequestMapping(value = "/addNewUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> restMethod(String firstName, String lastName, String role, String email, String password) {
        User user = new User(firstName, lastName, email,password);
        user = userRepository.save(user);

        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
@Autowired
    RoleRepository roleRepository;
    @RequestMapping(value = "/getAllUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userRepository.findAll();
        if(users == null){
            return new ResponseEntity( null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }



    @GetMapping(value = "users/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable String userID){
        Optional<User> optionalUser = userRepository.findById(userID);
        if (optionalUser.isEmpty()) {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }
        User user = optionalUser.get();
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @GetMapping(value = "users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> getUsers(@RequestParam(required = false, defaultValue = "0") Integer startIndex, @RequestParam(required = false, defaultValue = "0") Integer startIndexOffset, @RequestParam(required = false) String email, @RequestParam(required = false) String firstName, @RequestParam(required = false) String lastName) {
        List<User> teams = new ArrayList<User>();
        if (startIndex == null) {
            startIndex = 0;
        }
        if (startIndexOffset == null || startIndexOffset == 0) {
            startIndexOffset = (int) userRepository.count();
        }
        if (email != null && !email.equals("")) {
            User tmp = userRepository.findByEmail(email);
            teams.add(tmp);
        }
        if ((lastName != null && !lastName.equals(""))) {
            if(firstName != null && !firstName.equals("")) {
                List<User> tmp = userRepository.findByLastNameAndFirstName(lastName, firstName);
                teams.addAll(tmp.subList(startIndex > tmp.size() ? 0 : startIndex, startIndexOffset > tmp.size() ? tmp.size() : startIndexOffset));
            }else {
                List<User> tmp = userRepository.findByLastName(lastName);
                teams.addAll(tmp.subList(startIndex > tmp.size() ? 0 : startIndex, startIndexOffset > tmp.size() ? tmp.size() : startIndexOffset));
            }
        }else {
            if(firstName != null && !firstName.equals("")) {
                List<User> tmp = userRepository.findByFirstName(firstName);
                teams.addAll(tmp.subList(startIndex > tmp.size() ? 0 : startIndex, startIndexOffset > tmp.size() ? tmp.size() : startIndexOffset));
            }
        }
        if ((lastName == null || lastName.equals("")) && (email == null || email.equals("")) && (firstName == null || firstName.equals(""))) {
            teams.addAll(userRepository.findAll().subList(startIndex, startIndexOffset));
        }
        if (teams.size() == 0) {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(teams, HttpStatus.OK);
    }

    @PutMapping(value = "users/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateUser(@PathVariable String userID, @RequestBody User user) {
        Optional<User> optionalTeam = userRepository.findById(userID);
        if (optionalTeam.isEmpty()) {
            return new ResponseEntity<User>(user, HttpStatus.NO_CONTENT);
        }
        user.setId(userID);
        user = userRepository.save(user);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @DeleteMapping(value = "users/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> deleteUser(@PathVariable String userID) {
        if (userRepository.existsById(userID)) {
            userRepository.deleteById(userID);
            return new ResponseEntity(null, HttpStatus.OK);
        }
        return new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }
}
