package com.scruum.mastersofscruum.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class SprintDto {
    public String id;
    @NotNull
    public String sprintName;
    @NotNull
    public String goal;
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    public LocalDate startDate;
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    public LocalDate endDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSprintName() {
        return sprintName;
    }

    public void setSprintName(String sprintName) {
        this.sprintName = sprintName;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public SprintDto() {
    }

    public SprintDto(String id, @NotNull String sprintName, @NotNull String goal, @NotNull LocalDate startDate, @NotNull LocalDate endDate) {
        this.id = id;
        this.sprintName = sprintName;
        this.goal = goal;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
