package com.scruum.mastersofscruum.mapper.Impl;

import com.scruum.mastersofscruum.dto.TeamDto;
import com.scruum.mastersofscruum.mapper.TeamMapper;
import com.scruum.mastersofscruum.model.Role;
import com.scruum.mastersofscruum.model.Team;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TeamMapperImpl implements TeamMapper {
    @Override
    public TeamDto toDto(Team team) {
        return TeamMapper.INSTANCE.toDto(team);
    }

    @Override
    public Team fromDto(TeamDto teamDto) {
        if (teamDto == null){
            return null;
        }
        Team team = new Team();
        if(teamDto.getId() != null && !teamDto.getId().equals("")) {
            team.setId(teamDto.getId());
        }
        if(teamDto.getMembers() == null){
            Map<Role, List<String>> map = new HashMap<>();
            map.put(Role.SCRUM_MASTER, new ArrayList<String>(1));
            map.put(Role.PRODUCT_OWNER, new ArrayList<String>(1));
            map.put(Role.TEAM, new ArrayList<String>());
            team.setMembers(map);
        }else{
            team.setMembers(teamDto.getMembers());
        }
        team.setSprintList(new ArrayList<>());
        team.setTeamName(teamDto.getTeamName());

        return team;
    }

    @Override
    public List<TeamDto> toDtoList(List<Team> teamList) {
        return TeamMapper.INSTANCE.toDtoList(teamList);
    }
}