package com.scruum.mastersofscruum.mapper;

import com.scruum.mastersofscruum.dto.SprintDto;
import com.scruum.mastersofscruum.model.Sprint;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SprintMapper {
    SprintMapper INSTANCE = Mappers.getMapper(SprintMapper.class);

    SprintDto toDto(Sprint sprint);

    Sprint fromDto(SprintDto sprintDto);

    List<SprintDto> toDtoList(List<Sprint> sprintList);
}
