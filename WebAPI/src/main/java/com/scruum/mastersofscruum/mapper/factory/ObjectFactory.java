package com.scruum.mastersofscruum.mapper.factory;

import java.lang.reflect.Field;

public class ObjectFactory {
    public static <T> T objectFactory(T obj, Class<T> objType) throws IllegalAccessException, InstantiationException {
        T tmp = objType.newInstance();
        Field[] fields = objType.getFields();
        for (Field field : fields) {
            try {
                if (field.get(obj).equals(null)) {
                    field.set(obj, field.get(tmp));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return obj;
    }
}
