package com.scruum.mastersofscruum.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class Notification {
    private int id;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime notification;
    private Long scheduled; // in minutes
}
