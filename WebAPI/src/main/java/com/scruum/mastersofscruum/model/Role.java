package com.scruum.mastersofscruum.model;

public enum Role {
    SCRUM_MASTER,
    PRODUCT_OWNER,
    TEAM
}
