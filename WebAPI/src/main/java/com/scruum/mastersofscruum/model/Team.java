package com.scruum.mastersofscruum.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;
import java.util.Map;

//@Getter
//@Setter
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
@Data
public class Team {
    @Id
    private String id;
    private String teamName;
    private Map<Role, List<String>> members;
    @JsonIgnore
    private List<String> sprintList;
    @JsonIgnore
    private List<Meeting> meetingList;

    public Team() {
    }

    public Team(String id, String teamName, Map<Role, List<String>> members, List<String> sprintList, List<Meeting> meetingList) {
        this.id = id;
        this.teamName = teamName;
        this.members = members;
        this.sprintList = sprintList;
        this.meetingList = meetingList;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

    public List<Meeting> getMeetingList() {
        return meetingList;
    }

    public void setMeetingList(List<Meeting> meetingList) {
        this.meetingList = meetingList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Map<Role, List<String>> getMembers() {
        return members;
    }

    public void setMembers(Map<Role, List<String>> members) {
        this.members = members;
    }

    public List<String> getSprintList() {
        return sprintList;
    }

    public void setSprintList(List<String> sprintList) {
        this.sprintList = sprintList;
    }
}
