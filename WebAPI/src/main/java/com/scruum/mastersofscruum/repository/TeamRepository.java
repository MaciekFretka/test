package com.scruum.mastersofscruum.repository;

import com.scruum.mastersofscruum.model.Team;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface TeamRepository extends MongoRepository<Team, String> {

    public List<Team> findByTeamName(String teamName);

    @Query(value = "{ $or: [{'members.SCRUM_MASTER': ?0}, {'members.PRODUCT_OWNER': ?0}, {'members.TEAM': ?0}]}")
    List<Team> findTeamByMember(String userId);
}
