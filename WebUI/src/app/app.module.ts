import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './route';
import { AppComponent } from './container/app.component';
import { AllUsersService } from './services/all-users.service';
import { GetAllUsersModule } from './modules/get-all-users/get-all-users.module';
import { LoginModule } from './modules/login/login.module';
import { RegisterModule } from './modules/register/register.module';
import { CreateTeamModule } from './modules/create-team/create-team.module';
import { GetAllTeamsModule } from './modules/get-all-teams/get-all-teams.module';
import { EditTeamModule } from './modules/edit-team/edit-team.module';
import { EditTeamComponent } from './modules/edit-team/edit-team.component';
import { SprintsModule } from './modules/sprints/sprints.module';
import { MeetingsModule } from './modules/meetings/meetings.module';
import { TeamsModule } from './modules/teams/teams.module';
import { GetMeetingsModule } from './modules/get-meetings/get-meetings.module';
import { GetMeetingsTeamModule } from './modules/get-meetings-team/get-meetings-team.module';
import { GetMeetingsSprintComponent } from './modules/get-meetings-sprint/get-meetings-sprint.component';
import {AuthInterceptor} from 'src/app/_helpers/auth.interceptor';
import { AuthGuard } from './auth.guard';



@NgModule({
  declarations: [
    AppComponent,
    GetMeetingsSprintComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    GetAllUsersModule,
    LoginModule,
    RegisterModule,
    GetAllUsersModule,
    CreateTeamModule,
    GetAllTeamsModule,
    GetMeetingsModule,
    GetMeetingsTeamModule,
    EditTeamModule,
    SprintsModule,
    MeetingsModule,
    TeamsModule
  ],
  providers: [{
    provide: APP_INITIALIZER,
    useFactory: (http: AllUsersService) => {
      return () => {
        return http.load();
      };
    },
    deps: [AllUsersService],
    multi: true
  },AuthGuard,{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
