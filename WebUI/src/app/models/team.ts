import { Members } from './members';

export interface Team{
    id: string;
    teamName: string;
    members: Members;
  }
