import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateTeamComponent } from './create-team.component';



@NgModule({
  declarations: [
    CreateTeamComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    FormsModule
  ]
})
export class CreateTeamModule { }
