import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { CreateTeamService } from 'src/app/services/team.service';
import { Team } from 'src/app/models/team';
import { Members } from 'src/app/models/members';
import { stringify } from 'querystring';
import { User } from 'src/app/models/user';
import { AllUsersService } from 'src/app/services/all-users.service';

@Component({
  selector: 'app-edit-team',
  templateUrl: './edit-team.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
  `]
})
export class EditTeamComponent implements OnInit {
  visible: boolean;
  canI: boolean;
  role: string;
  member: string;
  userID: string;
  alreadyUsed = new Map();
  filteredUsers: Array<string>;
  mapOfUsers = new Map();
  users: User[];
  newTeam: Team;
  listOfMembers = new Map<string, string>();
  teamID: string;

  constructor(private teamService: CreateTeamService, private route: ActivatedRoute, private userService: AllUsersService,
              private router: Router) {
    this.newTeam = { teamName: '', id: '', members: { SCRUM_MASTER: [], TEAM: [], PRODUCT_OWNER: []}};
  }

  ngOnInit(): void {
    this.teamID = this.route.snapshot.paramMap.get('id');
    this.teamService.getTeam(this.teamID).subscribe((result: Team) => {
      this.newTeam = result;
    });
    this.role = 'Developer';
    this.visible = false;
    this.userService.get().subscribe((ret) => {
      this.users = ret;
    });
    setTimeout(() => {
      this.intoString();
      this.newTeam.members.TEAM.forEach(member => {
        this.users.forEach(user => {
          if (user.id === member){
            this.alreadyUsed.set(user.email, 'Developer');
          }
        });
      });
      this.newTeam.members.PRODUCT_OWNER.forEach(member => {
        this.users.forEach(user => {
          if (user.id === member){
            this.alreadyUsed.set(user.email, 'Product Owner');
          }
        });
      });
    }, 2000);
  }

  intoString() {
    if (this.mapOfUsers.size === 0) {
      this.users.forEach((value) => {
        if (value.email != null) {
          this.mapOfUsers.set(value.id, value.email);
        }
      });
      console.log(this.mapOfUsers);
    }
  }

  fillInput(user: string) {
    this.member = user;
  }

  filter() {
    this.filteredUsers = [];
    this.mapOfUsers.forEach((value: string, key: string) => {
      if (value.startsWith(this.member)) {
        this.filteredUsers.push(value);
      }
    });
  }

  changeVisibility() {
    this.visible = !this.visible;
  }

  deleteMe(email: string, role: string) {
    this.alreadyUsed.delete(email);

    this.mapOfUsers.forEach((userEmail: string, id: string) => {
      if (userEmail === email) {
        this.userID = id;
      }
    });

    if (role === 'Developer') {
      const index = this.newTeam.members.TEAM.indexOf(this.userID, 0);
      if (index > -1) {
        this.newTeam.members.TEAM.splice(index, 1);
      }
    } else if (role === 'Product Owner') {
      const index = this.newTeam.members.PRODUCT_OWNER.indexOf(this.userID, 0);
      if (index > -1) {
        this.newTeam.members.PRODUCT_OWNER.splice(index, 1);
      }
    }
    console.log(this.newTeam);
  }

  addMember() {
    this.canI = false;

    this.mapOfUsers.forEach((value: string, key: string) => {
      if (value === this.member) {
        if (this.alreadyUsed.get(value) === undefined){
          this.canI = true;
          this.userID = key;
        }
      }
    });

    if (this.canI) {

      if (this.role === 'Developer') {
        this.newTeam.members.TEAM.push(this.userID);
        this.alreadyUsed.set(this.member, 'Developer');
      } else if (this.role === 'Product Owner') {
        this.newTeam.members.PRODUCT_OWNER.push(this.userID);
        this.alreadyUsed.set(this.member, 'Product Owner');
      }

      this.member = '';

      console.log(this.newTeam);
    }
  }

  deleteTeam(){
    this.teamService.deleteTeam(this.newTeam.id);
  }

  onSubmit(){
    console.log(this.newTeam);
    this.teamService.editTeam(this.newTeam);
  }

}
