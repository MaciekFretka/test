import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route } from '@angular/router';
import { CreateTeamService } from 'src/app/services/team.service';
import { Team } from 'src/app/models/team';
import { Members } from 'src/app/models/members';

@Component({
  selector: 'app-get-all-teams',
  templateUrl: './get-all-teams.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
  `]
})
export class GetAllTeamsComponent implements OnInit {
  teamList: Array<Team> = new Array<Team>();

  constructor(private teamService: CreateTeamService) {
  }
    ngOnInit(): void {
        // todo
    }

    add(){
        const team: Team = { id: '1', teamName: 'name', members: { SCRUM_MASTER: ['lol'], PRODUCT_OWNER: ['xd'], TEAM: ['pl'] } };
        this.teamList.push(team);
        console.log(this.teamList);
    }
}
