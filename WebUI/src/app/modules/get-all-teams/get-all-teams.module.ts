import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetAllTeamsComponent } from './get-all-teams.component';



@NgModule({
  declarations: [
    GetAllTeamsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    FormsModule
  ]
})
export class GetAllTeamsModule { }
