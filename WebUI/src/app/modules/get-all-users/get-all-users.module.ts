import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetAllUsersComponent } from './get-all-users.component';



@NgModule({
  declarations: [
    GetAllUsersComponent
  ],
  imports: [
    CommonModule,
  ]
})
export class GetAllUsersModule { }
