import { Component, OnInit } from '@angular/core';
import { Meeting } from 'src/app/models/meeting';
import { GetMeetingsService } from '../../services/get-meetings.service';

@Component({
  selector: 'app-get-meetings-team',
  templateUrl: './get-meetings-team.component.html',
  styleUrls: ['./get-meetings-team.component.css']
})
export class GetMeetingsTeamComponent implements OnInit {
  public test: Meeting[];
  constructor(private getMeetingsSevice : GetMeetingsService) { }

  ngOnInit(): void {
    this.getMeetingsSevice.loadTeam().subscribe((ret) => {
      this.test = ret.filter(mett => {
        let year_met = new Date(mett.startDateTime).getFullYear()
        let month_met= new Date(mett.startDateTime).getMonth()
        let day_met= new Date(mett.startDateTime).getDay()
       // console.log(year_met)
       // console.log(month_met)
      //  console.log(day_met)
       
      let year = new Date().getFullYear()
        //console.log(year) 
      let month =new Date().getMonth()
        //console.log(month)
      let day =new Date().getDay()
      //console.log(day)
      if(year==year_met && month==month_met && day==day_met){
        return true
       }else{
        return false
       }
      });
    });
  }

}
