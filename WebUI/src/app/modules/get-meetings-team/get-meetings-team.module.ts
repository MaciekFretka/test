import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetMeetingsTeamComponent } from './get-meetings-team.component';



@NgModule({
  declarations: [GetMeetingsTeamComponent],
  imports: [
    CommonModule
  ]
})
export class GetMeetingsTeamModule { }
