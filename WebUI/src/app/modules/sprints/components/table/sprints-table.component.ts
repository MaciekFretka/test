import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route } from '@angular/router';
import { User } from 'src/app/models/user';
import { LoginService } from 'src/app/services/login.service';
import { Sprint } from 'src/app/models/sprint';
import { GridComponent } from '@syncfusion/ej2-angular-grids';

@Component({
  selector: 'app-sprints-table-component',
  templateUrl: './sprints-table.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
  `]
})
export class SprintsTableComponent{
  @Input() sprints: Sprint[];
  @Output() selected: EventEmitter<Sprint> = new EventEmitter();
  @ViewChild('grid', {static: true}) public grid: GridComponent;

  public select(): void {
    const select: Sprint[] = this.grid.getSelectedRecords() as Sprint[];
    this.selected.emit(select[0]);
  }
}
