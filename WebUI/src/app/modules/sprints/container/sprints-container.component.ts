import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { SprintService } from 'src/app/services/sprint.service';
import { Sprint } from 'src/app/models/sprint';
import { tap, map } from 'rxjs/operators';
import { Members } from 'src/app/models/members';
import { CreateTeamService } from 'src/app/services/team.service';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'app-sprints-container',
  templateUrl: './sprints-container.component.html',
  styles: [`
    .all-screen { width: 100%; height: 100%; }
    .margin{margin: 25px;}
  `]
})
export class SprintsComponent implements OnInit{
  public selectedSprint: Sprint;
  public sprints: Sprint[];
  public teamId: string;
  public team: Team;

  constructor(private sprintService: SprintService, private teamService: CreateTeamService, private route: ActivatedRoute){
    this.sprints = [] as Sprint[];
    this.selectedSprint = {} as Sprint;
    this.team = {} as Team;
  }

  ngOnInit() {
    this.teamId = this.route.snapshot.paramMap.get('id');
    this.sprintService.load(this.teamId).subscribe((result: Sprint[]) => {
      this.sprints = result;
    });
    this.teamService.getTeam(this.teamId).subscribe((team: Team) => {
      this.team = team;
    });
  }

  add() {
    this.selectedSprint = {} as Sprint;
  }

  async del() {
    await this.sprintService.remove(this.teamId, this.selectedSprint).toPromise();
    await this.refresh();
  }

  selected(sprint: Sprint): void{
    this.selectedSprint = sprint;
  }

  async save(sprint: Sprint) {
    this.selectedSprint = {} as Sprint;
    if (this.sprints !== null && this.sprints.filter(x => x.id === sprint.id).length !== 0) {
      await this.sprintService.update(this.teamId, sprint).toPromise();
    } else {
      await this.sprintService.create(this.teamId, sprint).toPromise();
    }
    await this.refresh();
  }

  async refresh() {
    const res = this.sprintService.load(this.teamId);
    await res.toPromise();
    res.subscribe((result: Sprint[]) => {
      this.sprints = result;
    });
  }
}
