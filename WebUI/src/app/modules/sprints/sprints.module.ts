import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SprintsComponent } from './container/sprints-container.component';
import { SprintsDetailComponent } from './components/detial/sprints-detail.component';
import { SprintsTableComponent } from './components/table/sprints-table.component';
import { GridModule } from '@syncfusion/ej2-angular-grids';
import { PageService, SortService, FilterService, GroupService } from '@syncfusion/ej2-angular-grids';
import { MeetingsModule } from '../meetings/meetings.module';
import { MeetingsComponent } from '../meetings/container/meetings-container.component';



@NgModule({
  declarations: [
    SprintsComponent,
    SprintsDetailComponent,
    SprintsTableComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    GridModule,
    MeetingsModule,
  ],
  exports: [
    FormsModule
  ],
  providers: [
    PageService,
    SortService,
    FilterService,
    GroupService]
})
export class SprintsModule { }
