import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route } from '@angular/router';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'app-teams-table-component',
  templateUrl: './teams-table.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
  `]
})
export class TeamsTableComponent{
  @Input() teams: Team[];
  @Output() selected: EventEmitter<Team> = new EventEmitter();
  @ViewChild('grid', {static: true}) public grid: GridComponent;

  public select(): void {
    const select: Team[] = this.grid.getSelectedRecords() as Team[];
    this.selected.emit(select[0]);
  }
}
