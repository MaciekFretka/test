import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../modules/login/login.component';
import { GetAllUsersComponent } from '../modules/get-all-users/get-all-users.component';
import { RegisterComponent } from '../modules/register/register.component';
import { CreateTeamComponent } from '../modules/create-team/create-team.component';
import { GetAllTeamsComponent } from '../modules/get-all-teams/get-all-teams.component';
import { EditTeamComponent } from '../modules/edit-team/edit-team.component';
import { SprintsComponent } from '../modules/sprints/container/sprints-container.component';
import { MeetingsComponent } from '../modules/meetings/container/meetings-container.component';
import { TeamsComponent } from '../modules/teams/container/teams-container.component';
import { GetMeetingsComponent } from '../modules/get-meetings/get-meetings.component';
import { GetMeetingsTeamComponent } from '../modules/get-meetings-team/get-meetings-team.component';
import {GetMeetingsSprintComponent  } from '../modules/get-meetings-sprint/get-meetings-sprint.component';
import { GetMeetingsTeamModule } from '../modules/get-meetings-team/get-meetings-team.module';
import { GetMeetingsSprintModule } from '../modules/get-meetings-sprint/get-meetings-sprint.module';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'getAllUsers',
    component: GetAllUsersComponent,
  },
  {
    path: 'getAllTeams',
    component: GetAllTeamsComponent,
  },
  {
    path: 'editTeam/:id',
    component: EditTeamComponent,
  },
  {
    path: 'sprint/:id',
    component: SprintsComponent,
  },
  {
    path: 'getMeetings',
    component: GetMeetingsComponent,
  },
  {
      path: 'getSprintMeetings',
      component: GetMeetingsSprintComponent,
  },
  {
    path: 'getTeamMeetings',
    component: GetMeetingsTeamComponent,
  },
  {
    path: 'team/:id',
    component: TeamsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
