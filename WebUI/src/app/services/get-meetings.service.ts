import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meeting } from '../models/meeting';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators'; 


@Injectable({
  providedIn: 'root'
})
@Injectable()
export class GetMeetingsService {
  
  private metings_user:Observable<Meeting[]>;
  private metings_team: Observable<Meeting[]>;
 
  constructor(private http: HttpClient) {}
 
  public loadUser():Observable<Meeting[]>{
    this.metings_user= this.http.get<Meeting[]>('http://localhost:8081/users/5eba8603a80f214399a749b4/meetings').pipe(filter(met=>{
    met.filter(mett =>{
  
let year_met = new Date(mett.startDateTime).getFullYear()
let month_met= new Date(mett.startDateTime).getMonth()
let day_met= new Date(mett.startDateTime).getDay()
console.log(year_met)
console.log(month_met)
console.log(day_met)
//////

let year = new Date().getFullYear()
console.log(year) 
let month =new Date().getMonth()
console.log(month)
let day =new Date().getDay()
console.log(day)

///////////
if(year==year_met && month==month_met && day==day_met){
 return true
}else{
 return false
}
    }) 
    return false;
    }));
  
 
    return this.metings_user;
  }

  public loadSprintMeeting():Observable<Meeting[]>{
    this.metings_user= this.http.get<Meeting[]>('http://localhost:8081/users/5eba8603a80f214399a749b4/teams/656/sprints/string/meetings')
    
    return this.metings_user;
  }
  
  public loadTodayMeeting(): Observable<Meeting[]>{
    this.metings_user= this.http.get<Meeting[]>('http://localhost:8081/users/5eba8603a80f214399a749b4/meetings')
    
    return this.metings_user;
  
  }
  public loadTeam(): Observable<Meeting[]>{
    this.metings_team = this.http.get<Meeting[]>('http://localhost:8081/teams/656/meetings');

    return this.metings_team;
  }
  
}






