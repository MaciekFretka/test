import { Injectable, Injector } from '@angular/core';
import { User } from '../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { isUndefined } from 'util';
import { map, catchError } from 'rxjs/operators';
import { TokenStorageService } from './token-storage.service';
import{JwtHelperService} from '@auth0/angular-jwt'

const AUTH_API='http://localhost:8081/api/auth';

const httpOptions={
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private user: Observable<User>;
  private u: User;
  helper = new JwtHelperService();
  constructor(private http: HttpClient) { }

  // public login(user: User): Observable<User>{
  //   this.user = this.http.get<User>('http://localhost:8081/api/auth?email=' + user.email + '&password=' + user.password);
  //   this.user.subscribe(r => this.u = r);
  //   console.log(this.u);
  //   return this.user;
  // }

 
 
 public login(user: User): Observable<User>{
  
    this.user=this.http.post<User>(AUTH_API+'/signin',{
      email: user.email,
      password: user.password
    }, httpOptions);
  return this.user;  
  }


  public logout(): void{
    this.user = undefined;
  }

  public register(user: User): Observable<User> {
    user.role = [] as string[]
    console.log(user.email)
    this.user = this.http.post<User>(AUTH_API+'/signup', {
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      password: user.password,
      roles: user.role
      

    });
    this.user.subscribe(r => this.u = r);
    return this.user;
  }

  public logged(): string{
    let userLogin: string;
    if (this.user !== undefined){
      this.user.subscribe(u => userLogin = u.firstName);
    }
    return userLogin;
  }
  loggedIn(){
    const token=sessionStorage.getItem('auth-token');
   return !this.helper.isTokenExpired(token);
   // return !!sessionStorage.getItem('auth-token')
    
  }
}
