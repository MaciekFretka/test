import { Injectable } from '@angular/core';
import { Team } from '../models/team';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class CreateTeamService {
  private team: Observable<Team>;
  private teams: Observable<Team[]>;
  private temTeam: any;
  private tem: any;
  constructor(private http: HttpClient) {}

  public createTeam(team: Team): Observable<Team> {
    console.log(team);
    this.http
      .post<Team>('http://localhost:8081/teams', team)
      .toPromise()
      .then((data) => {
        console.log(data);
      });
    return this.team;
  }

  public getAllTeams(): Observable<string> {
    return this.http.get<string>('http://localhost:8081/getAllTeams');
  }

  public getTeam(teamID: string): Observable<Team> {
    return this.http.get<Team>('http://localhost:8081/teams/' + teamID);
  }

  public editTeam(team: Team){
    console.log(team);
    this.http
      .put<Team>('http://localhost:8081/teams/' + team.id, team)
      .toPromise()
      .then((data) => {
        console.log(data);
      });
  }

  public deleteTeam(teamID: string) {
    this.http.delete('http://localhost:8081/teams/' + teamID).toPromise().then();
  }

  public getMyTeams(userID: string): Observable<Team[]>{
    this.teams = this.http.get<Team[]>('http://localhost:8081/users/' + userID + '/teams');
    return this.teams;
  }

  public addUserToTeam(team?: Team, user?: User) {
    this.http
      .put<Team>(
        'http://localhost:8081/addUserToTeam?teamID=' +
          '5e986449d8b89d68e0b9291c' +
          '&userID=' +
          '7261' +
          '&roleID=' +
          2,
        this.temTeam
      )
      .toPromise()
      .then((data) => {
        console.log(data);
      });
  }
}
